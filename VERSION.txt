16.0.0 Update to Angular 16.
15.0.0 Update to Angular 15.
14.0.0 Update to Angular 14.
13.0.3 Add method to support import of users
13.0.2 Add json error converter
13.0.1 Add local oidc users api
13.0.0 Update to Angular 13 and CI/CD optimization.
12.0.2 Fix mapping of the undefined roles, new version of the Sentinel.
12.0.1 Update gitlab CI
